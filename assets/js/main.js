$(document).ready(function () {
    $('.filter-btn').click(function () {
        if ($(this).hasClass('active')) {
            return;
        }

        $('.filter-btn.active').button('toggle');
        $(this).button('toggle');

        if ($(this).text() === 'All') {
            $('.card').show(300);
        } else {
            $('.card').hide().filter('[data-key="' + $(this).text().toLocaleLowerCase() + '"]')
                .show(300)
        }
    });

    $('.card').click(function() {
        $('body').toggleClass('stop-scroll');
        let image = $(this).find('.card-img').attr('src');
        let author = $(this).find('.bottom-block').html();
        let modal = $('#modal');
        modal.css('display', 'flex');
        modal.find('.image').attr('src', image);
        modal.find('.download-btn').attr('href', image);
        modal.find('.bottom-block').html(author);
    });

    $('#modal .close').click(function() {
        $('#modal').css('display', 'none');
        $('body').toggleClass('stop-scroll');
    });
});
