getPhotoObj = (dataKey, photoPath) => ({dataKey, photoPath});

getAuthorObj = (authorName, authorImg) => ({authorName, authorImg});

getPhotoBlock = (photo, author) => ({
        dataKey: photo.dataKey,
        photoPath: photo.photoPath,
        authorName: author.authorName,
        authorImg: author.authorImg
    });

const authorsArray = [
  getAuthorObj('Lida Doe', 'assets/images/face-1.jpg'),
  getAuthorObj('Sara Redetto', 'assets/images/face-2.jpg'),
  getAuthorObj('Mira Kashmir', 'assets/images/face-3.jpg'),
];

const photosArray = [
    getPhotoObj('pink', 'assets/images/img-1.jpg'),
    getPhotoObj('blue', 'assets/images/img-2.jpg'),
    getPhotoObj('yellow', 'assets/images/img-3.jpg'),
    getPhotoObj('pink', 'assets/images/img-4.jpg'),
    getPhotoObj('blue', 'assets/images/img-5.jpg'),
    getPhotoObj('yellow', 'assets/images/img-6.jpg'),
    getPhotoObj('yellow', 'assets/images/img-3.jpg'),
    getPhotoObj('pink', 'assets/images/img-4.jpg'),
];

const photoObjectsArray = [
    getPhotoBlock(photosArray[0], authorsArray[0]),
    getPhotoBlock(photosArray[1], authorsArray[1]),
    getPhotoBlock(photosArray[2], authorsArray[2]),
    getPhotoBlock(photosArray[3], authorsArray[1]),
    getPhotoBlock(photosArray[4], authorsArray[0]),
    getPhotoBlock(photosArray[1], authorsArray[2]),
    getPhotoBlock(photosArray[5], authorsArray[0]),
    getPhotoBlock(photosArray[2], authorsArray[1]),
    getPhotoBlock(photosArray[3], authorsArray[1]),
];


new Vue({
   el: '#app',
   data: {
     photoObjects: photoObjectsArray
   },
});
